
#define RED 			0
#define RED_BLINK 		1
#define GREEN 			2
#define GREEN_BLINK 	3

#define RED_PIN 		8
#define GREEN_PIN 		9

void setup()
{
	pinMode(13, OUTPUT);
	pinMode(8, OUTPUT);
	pinMode(9, OUTPUT);
	Serial.begin(115200);

	Serial.write('\n');
}

void loop()
{
	int reading, state = 0;
	static int i = 0;
	static boolean LED_on = false;

	i++;
	LED_on = !LED_on;  

	digitalWrite(13, LED_on);
	reading = analogRead(0);
	Serial.println( reading );
	delay(150);

	if( reading > 800)
		state = RED;
	else if( reading > 700)
		state = RED_BLINK;
	else if( reading > 500)
		state = GREEN;    
	else
		state = GREEN_BLINK; 

	if (i == 4)
	{
		if( state == GREEN_BLINK)
			digitalWrite(GREEN_PIN, LED_on);
		else
			digitalWrite(GREEN_PIN, LOW);

		if( state == RED_BLINK)
			digitalWrite(RED_PIN, LED_on);
		else
			digitalWrite(RED_PIN, LOW);

		i = 1;
	} 

	if( state == GREEN)
		digitalWrite(GREEN_PIN, HIGH);
	if( state == RED)
		digitalWrite(RED_PIN, HIGH);
}