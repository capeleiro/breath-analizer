/*
 * board.h
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef BOARD_H
#define BOARD_H

#ifndef	F_CPU
#define F_CPU		8000000UL
#endif

#include <stdint.h>

#define SENSOR_CHANNEL	0
#define LED0_BIT		6
#define LED1_BIT		5
#define LED2_BIT		4
#define LED3_BIT		3
#define LED4_BIT		2
#define LED5_BIT		1

#define BAUD_RATE		9600
#define ENABLE_DEBUG	0

#define MAX_READING			500
#define	ALCOHOL_SCALE		255
#define DELTA_SENS 			1
#define MAX_HOLD_COUNT		16
#define IGNORE_BASE_FACTOR	3
#define IDLE_DELTA			10

void warmup(void);
void led_init(void);
void led_set_state(uint8_t led, uint8_t state);
void led_set_pattern(uint8_t pattern);
uint8_t calculate_alcohol(int16_t reading, int16_t *base_in);
uint8_t calculate_pattern(uint8_t alcohol);

void periode_timer_init(void);
void periode_timer_reset(void);
uint8_t periode_timer_set(void);

#endif