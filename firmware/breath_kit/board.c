/*
 * board.c
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "board.h"
#include "buffer.h"
#include "serial.h"
#include "adc.h"
#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>


const uint8_t led_bit[6] = {LED0_BIT,LED1_BIT,LED2_BIT,LED3_BIT,LED4_BIT,LED5_BIT};
int16_t alcohol_base = 0;

void led_init(void){
	DDRA = (1<<LED0_BIT)|(1<<LED1_BIT)|(1<<LED2_BIT)|(1<<LED3_BIT)|(1<<LED4_BIT)|(1<<LED5_BIT);
}

/* Receives a binary value and turns on or off LEDs accordingly */
void led_set_pattern(uint8_t pattern)
{
	for (int i = 0; i < 6; ++i)
	{
		if(pattern & (1<<i))
			PORTA |= (1 << led_bit[i]);
		else
			PORTA &= ~(1 << led_bit[i]);
	}
}

uint8_t calculate_alcohol(int16_t reading, int16_t *base_in)
{
	static int16_t last_reading = 0;
	int16_t delta, alcohol;

	if(reading > MAX_READING + alcohol_base)
		reading = MAX_READING + alcohol_base;

	/* maps raw value on the range of the uotput value */
	alcohol = ((int32_t)(reading - alcohol_base) * ALCOHOL_SCALE) / MAX_READING;
	if (alcohol < 0)
		alcohol = 0;

	delta = reading - last_reading;
	last_reading = reading;
	/* adds variation in relation the last reading to a buffer */
	buffer_add( abs(delta) );

	/* If all the previous deltas in the buffer are below a certain value, the reading stabilized.
	 * The new base value is the current reading, so the alcohol content is considered zero, when
	 * the reading stabilizes */
	if (buffer_max() <= DELTA_SENS && alcohol < ALCOHOL_SCALE/IGNORE_BASE_FACTOR)
	{
		alcohol_base = reading;
	}

	*base_in = alcohol_base;

	return (uint8_t)alcohol;
}

/* Calculates the LED pattern given a alcohol value */
uint8_t calculate_pattern(uint8_t alcohol)
{
	static uint8_t last_pattern = 0, hold_count = 0;
	int16_t step = ALCOHOL_SCALE / 6;
	uint8_t current_pattern = 0, new_pattern;

	for (int i = 0; i < 6; ++i)
	{
		if (alcohol >= i*step)
		{
			current_pattern |= 1<<i;
		}
	}

	if( (last_pattern > current_pattern || (last_pattern == current_pattern && hold_count > 0) ) 
			&& hold_count < MAX_HOLD_COUNT)
	{ 
		hold_count++;
		new_pattern = last_pattern;
	}
	else
	{
		hold_count = 0;
		new_pattern = current_pattern;
	}

	last_pattern = new_pattern;

	return new_pattern;
}

void periode_timer_init(void)
{
	/* CTC Mode, with TOP on OCR1A */
	TCCR1A = (0<<WGM11)|(0<<WGM10);
	TCCR1B = (0<<WGM13)|(1<<WGM12);

	/* Prescaler 64, TOP = 16383 - T=131ms */
	TCCR1B |= (0<<CS12)|(1<<CS11)|(1<<CS10);
	OCR1A = 16383;
}

void periode_timer_reset(void)
{
	TIFR1 = 0xFF;
}

uint8_t periode_timer_set(void)
{
	return TIFR1 & (1<<OCF1A);
}


/* Loop until the sensor reading is stable */
void warmup()
{
	uint8_t pattern = 0xFF, rate = +1, i = 0;
	int16_t reading, base = 0;

	led_set_pattern(0xFF);
	_delay_ms(2000);

	/* The reading is considered stable, when a new base is created.
	 * This only happens when reading is stable for some consecutive times */
	while ( base == 0 )
	{
		/* blinks the LEDs */
		pattern = ~pattern;

		led_set_pattern(pattern);
		_delay_ms(400);

		adc_convert(&reading);
		calculate_alcohol(reading, &base);

		/* debug */
		adc_print_decimal(reading);
		serial_send('\t');
		adc_print_decimal(base);
		serial_send('\n');
	}
}