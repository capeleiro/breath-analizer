/*
 * adc.h
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ADC_H_
#define ADC_H_

#include <stdint.h>

#define ADC_REF_VCC 			0
#define ADC_REF_AREF			1
#define ADC_REF_INTERNAL1V1		2

int adc_init(void);
int adc_channel_set(uint8_t channel);
int adc_convert(int16_t *out);
void adc_print_decimal(int16_t dec);

#endif /* ADC_H_ */