/*
 * serial.h
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef	SERIAL_H
#define	SERIAL_H

#ifndef	F_CPU
#define F_CPU		8000000UL
#endif

#include <avr/io.h>

#define BAUD_RATE	9600
#define	WIDTH		1E6/BAUD_RATE

void serial_setup( uint8_t tx_in, volatile uint8_t *ddr_in, volatile uint8_t *port_in);
void serial_send(uint8_t data);
void serial_send_string(const char *string);
void adc_print_decimal(int16_t dec);

#endif