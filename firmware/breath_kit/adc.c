/*
 * adc.c
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include <avr/io.h>
#include <adc.h>

int adc_init(void)
{
	ADCSRA = (1 << ADEN) | /* Enable the ADC */
			 (1 << ADSC) | /* Start the first conversion... */
			 (1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0); /* Set clock to FCP/64 (prescaler 64) */

	ADMUX = (0 << REFS1) | (0 << REFS0); /* Voltage ref as default: VCC (+5V) */
	while (ADCSRA & (1 << ADSC)); /* Wait for the first conversion to complete */

	return 0;
}

int adc_channel_set(uint8_t channel)
{
    int ret = 0;

    if (channel >= 8) {
        ret = -1;
        goto out;
    }

    ADMUX = (ADMUX & 0xE0) | /* Clear all channel bits */
        channel;

out:
    return ret;
}

int adc_convert(int16_t *out)
{
    ADCSRA |= 1 << ADSC; /* Start the conversion... */
    while (ADCSRA & (1 << ADSC)); /* ...and wait for its completion */

    *out = (uint16_t)ADC;

    return 0;
}