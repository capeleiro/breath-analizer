/*
 * main.c
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define F_CPU		8000000UL

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "board.h"
#include "adc.h"
#include "buffer.h"
#include "serial.h"

int main(void)
{
	uint8_t pattern, alcohol, prev_alcohol = 0, idle_pattern = 0x55;
	int16_t reading, base, loop_count = 0, idle = 0;

	led_init();
	adc_init();
	adc_channel_set(SENSOR_CHANNEL);

	/* initialize the timer that generates the interval between readings of the sensor */
	periode_timer_init();

	/* initialize UART, with TX on PB0 */
	serial_setup(0, &DDRB, &PORTB);

	/* delay while the sensor heats up */
	warmup();

	for(;;)
	{
		periode_timer_reset();

		/* raw value of the sensor */
		adc_convert(&reading);
		/* calculated indicative value of alcohol */
		alcohol = calculate_alcohol(reading, &base);

		/* if the sensor is not idle, reset the idle counter */
		if( abs(alcohol - prev_alcohol) > IDLE_DELTA )
		{
			idle = 0;
		}	
		prev_alcohol = alcohol;	

		/* goes into idle mode if idle for 400 loops */
		if( ++idle > 400 )
		{
			if( loop_count++ == 10 ) {
				idle_pattern = ~idle_pattern;
				loop_count = 0;
			}
			led_set_pattern(idle_pattern);
		}
		/* displays the calculated pattern otherwise */
		else
		{
			pattern = calculate_pattern(alcohol);
			led_set_pattern(pattern);
		}

		/* debug */
		adc_print_decimal(reading);
		serial_send('\t');
		adc_print_decimal(base);
		serial_send('\n');

		/* wait until is time for another reading */
		while(!periode_timer_set());
	}

	return 0;
}