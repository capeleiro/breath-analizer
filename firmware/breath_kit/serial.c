/*
 * serial.c
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "serial.h"
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>

volatile uint8_t *port, *ddr;
uint8_t tx_bit;

/* Initialization Function */
void serial_setup( uint8_t tx_in, volatile uint8_t *ddr_in, volatile uint8_t *port_in)
{
	ddr = ddr_in;
	port = port_in;
	tx_bit = tx_in;

    *ddr |= (1<<tx_bit);
    *port |= (1<<tx_bit);
}

/* Send one byte */
void serial_send(uint8_t data)
{
    uint8_t mask = 1;

    /* Start Bit */
    *port &= ~(1<<tx_bit);
    _delay_us(WIDTH);

    do{
        if (data & mask)
 			*port |= (1<<tx_bit);
        else
 			*port &= ~(1<<tx_bit);

		mask = mask << 1;
		_delay_us(WIDTH);
    } while(mask);

    /* Stop Bit */
    *port |= (1<<0);
    _delay_us(2*WIDTH);
}

/* Send String */
void serial_send_string(const char *string)
{
    while(*string){
        serial_send((uint8_t)(*string));
        string++;
    }
}

void adc_print_decimal(int16_t dec)
{
	char str[16];
	itoa(dec, str, 10);
	serial_send_string(str);
}
