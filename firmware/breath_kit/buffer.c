/*
 * buffer.c
 *
 * Copyright (C) 2015 Rodrigo Capeleiro, HackerSchool
 *
 * This program is part of the Breath Analizer firmware
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "buffer.h"

int16_t buffer[BUFFER_SIZE];
uint8_t buffer_initialized = 0, buffer_index = 0;

void buffer_add(int16_t data)
{
	if(!buffer_initialized){
		buffer_initialized = !buffer_initialized;

		for (int i = 0; i < BUFFER_SIZE; ++i)
		{
			buffer[i] = data;
		}
	}

	buffer[buffer_index] = data;
	buffer_index++;
	if (buffer_index > BUFFER_SIZE - 1)
		buffer_index = 0;
}

int16_t buffer_max(void)
{
	int16_t max = -32768;
	for (int i = 0; i < BUFFER_SIZE; ++i)
	{
		if (buffer[i] > max)
			max = buffer[i];
	}
	return max;
}

int16_t buffer_min(void)
{
	int16_t min = 32767;
	for (int i = 0; i < BUFFER_SIZE; ++i)
	{
		if (buffer[i] < min)
			min = buffer[i];
	}
	return min;
}

int16_t buffer_get_data(uint8_t buffer_index)
{
	return buffer[buffer_index];
}